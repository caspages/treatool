<?php
/**
 * @file
 * edition_annotator_tool.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function edition_annotator_tool_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'edition_viewer';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Collection Viewer';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'pagerer';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['preset'] = 'edition_pager';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Footer: Global: Block area */
  $handler->display->display_options['footer']['block']['id'] = 'block';
  $handler->display->display_options['footer']['block']['table'] = 'views';
  $handler->display->display_options['footer']['block']['field'] = 'block';
  $handler->display->display_options['footer']['block']['block_to_insert'] = 'text_resize:0';
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['label'] = 'Comments Footer';
  $handler->display->display_options['footer']['area']['content'] = '[ajax_comment][list_comments]';
  $handler->display->display_options['footer']['area']['format'] = 'full_html';
  $handler->display->display_options['footer']['area']['tokenize'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_item_image']['id'] = 'field_item_image';
  $handler->display->display_options['fields']['field_item_image']['table'] = 'field_data_field_item_image';
  $handler->display->display_options['fields']['field_item_image']['field'] = 'field_item_image';
  $handler->display->display_options['fields']['field_item_image']['label'] = '';
  $handler->display->display_options['fields']['field_item_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_item_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_item_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_item_image']['type'] = 'starplugins_cloudzoom';
  $handler->display->display_options['fields']['field_item_image']['settings'] = array(
    'small_image_style' => 'larger_style',
    'zoom_image_style' => '0',
    'zoom_props' => 'zoomPosition: \'inside\', autoInside: true',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class=\'span6 clearfix\'>[field_item_image]</div><div class=\'span6 clearfix text-border\'>[body]</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: Views: Views Conditional */
  $handler->display->display_options['fields']['views_conditional']['id'] = 'views_conditional';
  $handler->display->display_options['fields']['views_conditional']['table'] = 'views_conditional';
  $handler->display->display_options['fields']['views_conditional']['field'] = 'views_conditional';
  $handler->display->display_options['fields']['views_conditional']['label'] = '';
  $handler->display->display_options['fields']['views_conditional']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['views_conditional']['if'] = 'body';
  $handler->display->display_options['fields']['views_conditional']['condition'] = '5';
  $handler->display->display_options['fields']['views_conditional']['then'] = '<div class=\'span12 image-center\'>[field_item_image]</div><div class=\'grid_12 image-center\'>[title]</div>';
  $handler->display->display_options['fields']['views_conditional']['or'] = '<div class="half-and-half"><div class=\'left-half image-center\'>[field_item_image]</div><div class=\'right-half transcription-text\'>[body]</div></div><div class=\'grid_5 image-center\'>[title]</div>';
  $handler->display->display_options['fields']['views_conditional']['strip_tags'] = 0;
  /* Field: Content: AJAX Add Comment */
  $handler->display->display_options['fields']['ajax_comment']['id'] = 'ajax_comment';
  $handler->display->display_options['fields']['ajax_comment']['table'] = 'node';
  $handler->display->display_options['fields']['ajax_comment']['field'] = 'ajax_comment';
  $handler->display->display_options['fields']['ajax_comment']['label'] = '';
  $handler->display->display_options['fields']['ajax_comment']['exclude'] = TRUE;
  $handler->display->display_options['fields']['ajax_comment']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['ajax_comment']['alter']['text'] = '<br/>
[ajax_comment]';
  $handler->display->display_options['fields']['ajax_comment']['element_class'] = 'span12';
  $handler->display->display_options['fields']['ajax_comment']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['ajax_comment']['submit_action'] = '1';
  /* Field: Content: List of comments */
  $handler->display->display_options['fields']['list_comments']['id'] = 'list_comments';
  $handler->display->display_options['fields']['list_comments']['table'] = 'node';
  $handler->display->display_options['fields']['list_comments']['field'] = 'list_comments';
  $handler->display->display_options['fields']['list_comments']['label'] = '';
  $handler->display->display_options['fields']['list_comments']['exclude'] = TRUE;
  $handler->display->display_options['fields']['list_comments']['element_class'] = 'span12';
  $handler->display->display_options['fields']['list_comments']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['list_comments']['ajax_comments_display_add_comment_form'] = TRUE;
  /* Sort criterion: Content: Display number (field_item_display_number) */
  $handler->display->display_options['sorts']['field_item_display_number_value']['id'] = 'field_item_display_number_value';
  $handler->display->display_options['sorts']['field_item_display_number_value']['table'] = 'field_data_field_item_display_number';
  $handler->display->display_options['sorts']['field_item_display_number_value']['field'] = 'field_item_display_number_value';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Contextual filter: Content: Collection Title (field_item_collection) */
  $handler->display->display_options['arguments']['field_item_collection_target_id']['id'] = 'field_item_collection_target_id';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['table'] = 'field_data_field_item_collection';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['field'] = 'field_item_collection_target_id';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'item' => 'item',
  );

  /* Display: Collection Block */
  $handler = $view->new_display('block', 'Collection Block', 'block');

  /* Display: New Window Link */
  $handler = $view->new_display('block', 'New Window Link', 'navigation_new_tab_link_block');
  $handler->display->display_options['display_description'] = 'The block which outputs a link to the current page in a new tab.';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['class'] = 'menu';
  $handler->display->display_options['style_options']['wrapper_class'] = 'content';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['footer'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_item_collection_target_id']['id'] = 'field_item_collection_target_id';
  $handler->display->display_options['relationships']['field_item_collection_target_id']['table'] = 'field_data_field_item_collection';
  $handler->display->display_options['relationships']['field_item_collection_target_id']['field'] = 'field_item_collection_target_id';
  $handler->display->display_options['relationships']['field_item_collection_target_id']['label'] = 'referenced from field_item_collection';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['relationship'] = 'field_item_collection_target_id';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['exclude'] = TRUE;
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '0';
  $handler->display->display_options['fields']['counter']['reverse'] = 0;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="[path]?page=[counter]" class="view-in-new-tab" target="_blank" title="press to open the collection in a new browser tab or window">view in new tab</a>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Collection Title (field_item_collection) */
  $handler->display->display_options['arguments']['field_item_collection_target_id']['id'] = 'field_item_collection_target_id';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['table'] = 'field_data_field_item_collection';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['field'] = 'field_item_collection_target_id';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_item_collection_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['block_description'] = 'View in new tab';
  $export['edition_viewer'] = $view;

  $view = new view();
  $view->name = 'editions';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Collections';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Collections';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access administration pages';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'body' => 'body',
    'field_edition_attribution' => 'field_edition_attribution',
    'field_edition_license' => 'field_edition_license',
  );
  $handler->display->display_options['style_options']['default'] = 'title';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'body' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_edition_attribution' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_edition_license' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Subject */
  $handler->display->display_options['fields']['field_collection_subject']['id'] = 'field_collection_subject';
  $handler->display->display_options['fields']['field_collection_subject']['table'] = 'field_data_field_collection_subject';
  $handler->display->display_options['fields']['field_collection_subject']['field'] = 'field_collection_subject';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = 'Description';
  /* Field: Content: Creator */
  $handler->display->display_options['fields']['field_collection_creator']['id'] = 'field_collection_creator';
  $handler->display->display_options['fields']['field_collection_creator']['table'] = 'field_data_field_collection_creator';
  $handler->display->display_options['fields']['field_collection_creator']['field'] = 'field_collection_creator';
  /* Field: Content: Source */
  $handler->display->display_options['fields']['field_collection_source']['id'] = 'field_collection_source';
  $handler->display->display_options['fields']['field_collection_source']['table'] = 'field_data_field_collection_source';
  $handler->display->display_options['fields']['field_collection_source']['field'] = 'field_collection_source';
  /* Field: Content: Publisher */
  $handler->display->display_options['fields']['field_collection_publisher']['id'] = 'field_collection_publisher';
  $handler->display->display_options['fields']['field_collection_publisher']['table'] = 'field_data_field_collection_publisher';
  $handler->display->display_options['fields']['field_collection_publisher']['field'] = 'field_collection_publisher';
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_collection_date']['id'] = 'field_collection_date';
  $handler->display->display_options['fields']['field_collection_date']['table'] = 'field_data_field_collection_date';
  $handler->display->display_options['fields']['field_collection_date']['field'] = 'field_collection_date';
  /* Field: Content: Contributor */
  $handler->display->display_options['fields']['field_collection_contributor']['id'] = 'field_collection_contributor';
  $handler->display->display_options['fields']['field_collection_contributor']['table'] = 'field_data_field_collection_contributor';
  $handler->display->display_options['fields']['field_collection_contributor']['field'] = 'field_collection_contributor';
  /* Field: Content: Rights */
  $handler->display->display_options['fields']['field_collection_rights']['id'] = 'field_collection_rights';
  $handler->display->display_options['fields']['field_collection_rights']['table'] = 'field_data_field_collection_rights';
  $handler->display->display_options['fields']['field_collection_rights']['field'] = 'field_collection_rights';
  /* Field: Content: Relation */
  $handler->display->display_options['fields']['field_collection_relation']['id'] = 'field_collection_relation';
  $handler->display->display_options['fields']['field_collection_relation']['table'] = 'field_data_field_collection_relation';
  $handler->display->display_options['fields']['field_collection_relation']['field'] = 'field_collection_relation';
  /* Field: Content: Format */
  $handler->display->display_options['fields']['field_collection_format']['id'] = 'field_collection_format';
  $handler->display->display_options['fields']['field_collection_format']['table'] = 'field_data_field_collection_format';
  $handler->display->display_options['fields']['field_collection_format']['field'] = 'field_collection_format';
  /* Field: Content: Language */
  $handler->display->display_options['fields']['field_collection_language']['id'] = 'field_collection_language';
  $handler->display->display_options['fields']['field_collection_language']['table'] = 'field_data_field_collection_language';
  $handler->display->display_options['fields']['field_collection_language']['field'] = 'field_collection_language';
  /* Field: Content: Type */
  $handler->display->display_options['fields']['field_collection_type']['id'] = 'field_collection_type';
  $handler->display->display_options['fields']['field_collection_type']['table'] = 'field_data_field_collection_type';
  $handler->display->display_options['fields']['field_collection_type']['field'] = 'field_collection_type';
  /* Field: Content: Identifier */
  $handler->display->display_options['fields']['field_collection_identifier']['id'] = 'field_collection_identifier';
  $handler->display->display_options['fields']['field_collection_identifier']['table'] = 'field_data_field_collection_identifier';
  $handler->display->display_options['fields']['field_collection_identifier']['field'] = 'field_collection_identifier';
  /* Field: Content: Coverage */
  $handler->display->display_options['fields']['field_collection_coverage']['id'] = 'field_collection_coverage';
  $handler->display->display_options['fields']['field_collection_coverage']['table'] = 'field_data_field_collection_coverage';
  $handler->display->display_options['fields']['field_collection_coverage']['field'] = 'field_collection_coverage';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'collection' => 'collection',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/collections';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Collections';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['editions'] = $view;

  $view = new view();
  $view->name = 'pages';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Items';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Items';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access administration pages';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'views_bulk_operations' => 'views_bulk_operations',
    'title' => 'title',
    'field_item_collection' => 'field_item_collection',
    'field_item_display_number' => 'field_item_display_number',
    'field_item_image' => 'field_item_image',
    'body' => 'body',
    'edit_node' => 'edit_node',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_item_collection' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_item_display_number' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_item_image' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'body' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<em>No results</em>.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Field: Bulk operations: Content */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'skip_permission_check' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'log' => 0,
      ),
    ),
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Item Title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Collection Title */
  $handler->display->display_options['fields']['field_item_collection']['id'] = 'field_item_collection';
  $handler->display->display_options['fields']['field_item_collection']['table'] = 'field_data_field_item_collection';
  $handler->display->display_options['fields']['field_item_collection']['field'] = 'field_item_collection';
  $handler->display->display_options['fields']['field_item_collection']['settings'] = array(
    'bypass_access' => 0,
    'link' => 1,
  );
  /* Field: Content: Display number */
  $handler->display->display_options['fields']['field_item_display_number']['id'] = 'field_item_display_number';
  $handler->display->display_options['fields']['field_item_display_number']['table'] = 'field_data_field_item_display_number';
  $handler->display->display_options['fields']['field_item_display_number']['field'] = 'field_item_display_number';
  $handler->display->display_options['fields']['field_item_display_number']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_item_image']['id'] = 'field_item_image';
  $handler->display->display_options['fields']['field_item_image']['table'] = 'field_data_field_item_image';
  $handler->display->display_options['fields']['field_item_image']['field'] = 'field_item_image';
  $handler->display->display_options['fields']['field_item_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_item_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = 'Original Document';
  $handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '200',
  );
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = 'Edit';
  /* Sort criterion: Content: Display number (field_item_display_number) */
  $handler->display->display_options['sorts']['field_item_display_number_value']['id'] = 'field_item_display_number_value';
  $handler->display->display_options['sorts']['field_item_display_number_value']['table'] = 'field_data_field_item_display_number';
  $handler->display->display_options['sorts']['field_item_display_number_value']['field'] = 'field_item_display_number_value';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'item' => 'item',
  );
  /* Filter criterion: Content: Collection Title (field_item_collection) */
  $handler->display->display_options['filters']['field_item_collection_target_id']['id'] = 'field_item_collection_target_id';
  $handler->display->display_options['filters']['field_item_collection_target_id']['table'] = 'field_data_field_item_collection';
  $handler->display->display_options['filters']['field_item_collection_target_id']['field'] = 'field_item_collection_target_id';
  $handler->display->display_options['filters']['field_item_collection_target_id']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_item_collection_target_id']['expose']['operator_id'] = 'field_item_collection_target_id_op';
  $handler->display->display_options['filters']['field_item_collection_target_id']['expose']['label'] = 'Filter by collection';
  $handler->display->display_options['filters']['field_item_collection_target_id']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['field_item_collection_target_id']['expose']['operator'] = 'field_item_collection_target_id_op';
  $handler->display->display_options['filters']['field_item_collection_target_id']['expose']['identifier'] = 'field_item_collection_target_id';
  $handler->display->display_options['filters']['field_item_collection_target_id']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Filter by item title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/items';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Items';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['pages'] = $view;

  return $export;
}
