<?php
/**
 * @file
 * edition_annotator_tool.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function edition_annotator_tool_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dublic_core_view|node|collection|default';
  $field_group->group_name = 'group_dublic_core_view';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'collection';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Dublin Core',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_collection_subject',
      2 => 'field_collection_creator',
      3 => 'field_collection_source',
      4 => 'field_collection_publisher',
      5 => 'field_collection_date',
      6 => 'field_collection_contributor',
      7 => 'field_collection_rights',
      8 => 'field_collection_relation',
      9 => 'field_collection_format',
      10 => 'field_collection_language',
      11 => 'field_collection_type',
      12 => 'field_collection_identifier',
      13 => 'field_collection_coverage',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Dublin Core',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-dublic-core-view field-group-fieldset',
        'description' => 'The Dublin Core metadata element set is common to all records, including items, files, and collections. For more information see, <a href=\'http://dublincore.org/documents/dces/\'>http://dublincore.org/documents/dces/</a>.',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_dublic_core_view|node|collection|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dublic_core|node|collection|form';
  $field_group->group_name = 'group_dublic_core';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'collection';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Dublin Core',
    'weight' => '2',
    'children' => array(
      0 => 'body',
      1 => 'field_collection_subject',
      2 => 'field_collection_creator',
      3 => 'field_collection_source',
      4 => 'field_collection_publisher',
      5 => 'field_collection_date',
      6 => 'field_collection_contributor',
      7 => 'field_collection_rights',
      8 => 'field_collection_relation',
      9 => 'field_collection_format',
      10 => 'field_collection_language',
      11 => 'field_collection_type',
      12 => 'field_collection_identifier',
      13 => 'field_collection_coverage',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Dublin Core',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-dublic-core field-group-fieldset',
        'description' => 'The Dublin Core metadata element set is common to all records, including items, files, and collections. For more information see, <a href=\'http://dublincore.org/documents/dces/\'>http://dublincore.org/documents/dces/</a>.',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_dublic_core|node|collection|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Dublin Core');

  return $field_groups;
}
