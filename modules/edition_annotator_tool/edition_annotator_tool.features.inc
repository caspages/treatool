<?php
/**
 * @file
 * edition_annotator_tool.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function edition_annotator_tool_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function edition_annotator_tool_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function edition_annotator_tool_node_info() {
  $items = array(
    'collection' => array(
      'name' => t('Collection'),
      'base' => 'node_content',
      'description' => t('Use <em>collection</em> to create collection of items.'),
      'has_title' => '1',
      'title_label' => t('Collection Title'),
      'help' => '',
    ),
    'item' => array(
      'name' => t('Item'),
      'base' => 'node_content',
      'description' => t('Use <em>item</em> to add collection items. Each item will contain the original text and a scanned image.'),
      'has_title' => '1',
      'title_label' => t('Item Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
