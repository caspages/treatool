TREATOOL for Drupal
=================================
TREATOOL (Text Reader, Editor, Annotator Tool) is a [Drupal 7](https://www.drupal.org/) feature set that allows researchers to share digitized texts. Images of the text are made available with the transcription next to it. Users can be given access to the site where they additionally can add notes while they are reviewing the texts.

The tool was firstly developed by [CASIT Web Services](http://casitwebservices.uoregon.edu/) at the [University of Oregon](http://uoregon.edu/) for [Heidi Kaufman of the English Department](http://english.uoregon.edu/profile/hkaufman).

See a [demo](https://treatool.uoregon.edu/) on Heidi Kaufman's current site.

Installation
------------
You need a Drupal 7 platform created with the UO Vanilla makefile. Save the makefile from one of the links below to the directory Drupal should be install in, then use [Drush](http://www.drush.org/) make to install the platform dependencies.

* At the University of Oregon: https://git.uoregon.edu/projects/UO_DRPL_PROD/repos/uovanilla_makefile/browse
* Bitbucket: https://bitbucket.org/caspages/uovanilla_public/src

Use Git to clone this repo into the platform's site directory (or download then extract it there). Follow the standard [fresh Drupal site installation](https://www.drupal.org/docs/7/install) steps.

Enable the theme CAS Corporate Clean and module TREATOOL.

How-To Guide
------------
[How-To Guide](https://docs.google.com/document/d/1jTh8D64UNcc0gwbY_fJHmC00kJQ54y_bxKP8XZcRxaw/edit?usp=sharing).

Current Features
----------------
* The ability to add and view text as images with transcription in a collection.
* The ability for authenticated users to add notes on items (image + transcription).

### Cloud Zoom

The images are displayed with a zoom feature. We are using [Cloud Zoom Drupal module](http://www.starplugins.com/cloudzoom/drupal) which includes a trial version of the JS file. Please purchase your own license of the plugin.


TODO/Future Features
--------------------
* Adding fluid navigation between items.
* Adding, viewing and exporting annotations on transcriptions for authenticated users.
* Moving the images (and their transcription?) to [iiif](http://iiif.io/) compatible image server.

Warning
-------
Use it as your own risk.

License
-------
Any custom code written by CASIT Web Services is published under the [MIT licence](LICENSE.txt).

For Drupal and third-party modules please review those module's licenses.

Contact
---------------------------
Maintainers of this repo [CASIT Web Services](http://casitwebservices.uoregon.edu/).

Copyright
---------
Copyright (c) 2017 Heidi Kaufman

