<?php

function cas_corporateclean_preprocess_html(&$variables) {
  // Add the google Font 'David Libre', sans serif to the head.
  drupal_add_css('https://fonts.googleapis.com/css?family=David+Libre', array('type' => 'external'));
}
